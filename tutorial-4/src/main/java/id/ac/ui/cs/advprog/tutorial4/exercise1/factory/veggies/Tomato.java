package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by sheilarezkia on 3/9/18.
 */
public class Tomato  implements Veggies {

    public String toString() {
        return "Tomatoes";
    }
}
