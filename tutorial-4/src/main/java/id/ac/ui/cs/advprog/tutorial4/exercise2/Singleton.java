package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    static Singleton objectSingleton;
    
    private Singleton() {
        objectSingleton = null;
    }

    public static Singleton getInstance() {
        if (objectSingleton == null) {
            objectSingleton = new Singleton();
        }
        return objectSingleton;
    }
}
