package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by sheilarezkia on 3/9/18.
 */
public class ChiliSauce implements Sauce {
    public String toString() {
        return "Chili Sauce";
    }
}
