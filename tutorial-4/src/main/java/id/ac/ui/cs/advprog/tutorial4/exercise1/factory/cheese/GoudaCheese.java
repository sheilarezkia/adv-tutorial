package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created by sheilarezkia on 3/9/18.
 */
public class GoudaCheese implements Cheese {
    public String toString() {
        return "Melted Gouda Cheese";
    }
}
