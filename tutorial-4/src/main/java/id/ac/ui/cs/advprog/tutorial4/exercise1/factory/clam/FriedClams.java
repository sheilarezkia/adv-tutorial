package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * Created by sheilarezkia on 3/9/18.
 */
public class FriedClams implements Clams {
    public String toString() {
        return "Fried Clams from Jimbaran Beach";
    }
}
