package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DoughTest {

    private Class<?> doughClass;
    private Class<?> thinCrustClass;
    private Class<?> thickCrustClass;
    private Class<?> noCrustClass;

    @Before
    public void setUp() throws Exception {
        doughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
        thinCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough");
        thickCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough");
        noCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int doughClassModifier = doughClass.getModifiers();

        assertTrue(Modifier.isPublic(doughClassModifier));
        assertTrue(Modifier.isInterface(doughClassModifier));
    }

    @Test
    public void testDoughToStringMethod() throws Exception {
        Method toStringMethod = doughClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testThinCrustDoughBehavior() {
        Collection<Type> thinCrustInterfaces = Arrays.asList(thinCrustClass.getInterfaces());

        assertTrue(thinCrustInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testThinCrustOverridesToString() throws Exception {
        Method toStringMethod = thinCrustClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testThickCrustDoughBehavior() {
        Collection<Type> thickCrustInterfaces = Arrays.asList(thickCrustClass.getInterfaces());

        assertTrue(thickCrustInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testThickCrustOverridesToString() throws Exception {
        Method toStringMethod = thickCrustClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testNoCrustDoughBehavior() {
        Collection<Type> noCrustInterfaces = Arrays.asList(noCrustClass.getInterfaces());

        assertTrue(noCrustInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testNoCrustOverridesToString() throws Exception {
        Method toStringMethod = noCrustClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }
}
