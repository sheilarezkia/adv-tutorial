package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheeseTest {

    private Class<?> cheeseClass;
    private Class<?> goudaCheeseClass;
    private Class<?> mozarellaCheeseClass;
    private Class<?> parmesanCheeseClass;
    private Class<?> reggianoCheeseClass;


    @Before
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
        goudaCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese");
        mozarellaCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
        parmesanCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese");
        reggianoCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese");
    }


    @Test
    public void testCheeseIsAPublicInterface() {
        int cheeseClassModifiers = cheeseClass.getModifiers();

        assertTrue(Modifier.isPublic(cheeseClassModifiers));
        assertTrue(Modifier.isInterface(cheeseClassModifiers));
    }

    @Test
    public void testCheeseToStringMethod() throws Exception {
        Method toStringMethod = goudaCheeseClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testGoudaCheeseCheeseBehavior() {
        Collection<Type> goudaCheeseInterfaces = Arrays.asList(goudaCheeseClass.getInterfaces());

        assertTrue(goudaCheeseInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese")));
    }

    @Test
    public void testGoudaCheeseOverridesToString() throws Exception {
        Method toStringMethod = goudaCheeseClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testMozarellaCheeseCheeseBehavior() {
        Collection<Type> mozarellaCheeseInterfaces = Arrays.asList(mozarellaCheeseClass.getInterfaces());

        assertTrue(mozarellaCheeseInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese")));
    }

    @Test
    public void testMozarellaCheeseOverridesToString() throws Exception {
        Method toStringMethod = mozarellaCheeseClass.getDeclaredMethod("toString");
        int toStrModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testParmesanCheeseCheeseBehavior() {
        Collection<Type> parmesanInterfaces = Arrays.asList(parmesanCheeseClass.getInterfaces());

        assertTrue(parmesanInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese")));
    }

    @Test
    public void testParmesanCheeseOverridesToString() throws Exception {
        Method toStringMethod = parmesanCheeseClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testReggianoCheeseCheeseBehavior() {
        Collection<Type> reggianoInterfaces = Arrays.asList(reggianoCheeseClass.getInterfaces());

        assertTrue(reggianoInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese")));
    }

    @Test
    public void testReggianoCheeseOverridesToString() throws Exception {
        Method toStringMethod = reggianoCheeseClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }
}
