package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class PizzaTest {

    private Class<?> pizzaClass;

    @Before
    public void setUp() throws Exception {
        pizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza");
    }

    @Test
    public void testPizzaClassIsAbstract() {
        int pizzaClassModifier = pizzaClass.getModifiers();

        assertTrue(Modifier.isAbstract(pizzaClassModifier));
    }

    @Test
    public void testPizzaBakeMethod() throws Exception {
        Method bakeMethod = pizzaClass.getDeclaredMethod("bake");
        int bakeMethodModifier = bakeMethod.getModifiers();

        assertTrue(Modifier.isPublic(bakeMethodModifier));
        assertEquals("void", bakeMethod.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaCutMethod() throws Exception {
        Method cutMethod = pizzaClass.getDeclaredMethod("cut");
        int cutMethodModifier = cutMethod.getModifiers();

        assertTrue(Modifier.isPublic(cutMethodModifier));
        assertEquals("void", cutMethod.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaBoxMethod() throws Exception {
        Method boxMethod = pizzaClass.getDeclaredMethod("box");
        int boxMethodModifier = boxMethod.getModifiers();

        assertTrue(Modifier.isPublic(boxMethodModifier));
        assertEquals("void", boxMethod.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaSetName() throws Exception {
        Method setNameMethod = pizzaClass.getDeclaredMethod("setName",
                String.class);
        int setNameMethodModifier = setNameMethod.getModifiers();

        assertTrue(Modifier.isPublic(setNameMethodModifier));
    }

    @Test
    public void testPizzaGetName() throws Exception {
        Method getNameMethod = pizzaClass.getDeclaredMethod("getName");
        int getNameMethodModifier = getNameMethod.getModifiers();

        assertTrue(Modifier.isPublic(getNameMethodModifier));
    }

    @Test
    public void testPizzaToStringMethod() throws Exception {
        Method toStringMethod = pizzaClass.getDeclaredMethod("toString");
        int toStringMethodModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringMethodModifier));
    }
}

