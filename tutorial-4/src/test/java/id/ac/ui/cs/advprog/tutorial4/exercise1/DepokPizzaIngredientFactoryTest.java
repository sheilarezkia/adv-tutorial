package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class DepokPizzaIngredientFactoryTest {
    private Class<?> dpkPizzaIngFactoryClass;

    @Before
    public void setUp() throws Exception {
        dpkPizzaIngFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory");
    }

    @Test
    public void testDpkPizzaIngFactoryPizzaIngredientFactoryBehavior() {
        Collection<Type> dpkPizzaInterfaces = Arrays.asList(dpkPizzaIngFactoryClass.getInterfaces());

        assertTrue(dpkPizzaInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory")));

    }

    @Test
    public void testDpkPizzaOverridesMethodCreateDough() throws Exception {
        Method createDoughMethod = dpkPizzaIngFactoryClass.getDeclaredMethod("createDough");
        int createDoughModifier = createDoughMethod.getModifiers();

        assertTrue(Modifier.isPublic(createDoughModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", createDoughMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testDpkPizzaOverridesMethodCreateSauce() throws Exception {
        Method createSauceMethod = dpkPizzaIngFactoryClass.getDeclaredMethod("createSauce");
        int createSauceModifier = createSauceMethod.getModifiers();

        assertTrue(Modifier.isPublic(createSauceModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", createSauceMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testDpkPizzaOverridesMethodCreateCheese() throws Exception {
        Method createCheeseMethod = dpkPizzaIngFactoryClass.getDeclaredMethod("createCheese");
        int createCheeseModifier = createCheeseMethod.getModifiers();

        assertTrue(Modifier.isPublic(createCheeseModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", createCheeseMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testDpkPizzaOverridesMethodCreateVeggies() throws Exception {
        Method createVeggiesMethod = dpkPizzaIngFactoryClass.getDeclaredMethod("createVeggies");
        int createVeggiesModifier = createVeggiesMethod.getModifiers();

        assertTrue(Modifier.isPublic(createVeggiesModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", createVeggiesMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testDpkPizzaOverridesMethodCreateClam() throws Exception {
        Method createClamMethod = dpkPizzaIngFactoryClass.getDeclaredMethod("createClam");
        int createClamModifier = createClamMethod.getModifiers();

        assertTrue(Modifier.isPublic(createClamModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", createClamMethod.getGenericReturnType().getTypeName());

    }

}
