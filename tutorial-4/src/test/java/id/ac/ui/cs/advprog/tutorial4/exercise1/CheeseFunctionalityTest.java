package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheeseFunctionalityTest {

    private Cheese goudaCheese;
    private Cheese mozzarellaCheese;
    private Cheese parmesanCheese;
    private Cheese regiannoCheese;

    @Before
    public void setUp() throws Exception {
        goudaCheese = new GoudaCheese();
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        regiannoCheese = new ReggianoCheese();
    }

    @Test
    public void testCheeseOutput(){
        assertEquals("Melted Gouda Cheese", goudaCheese.toString());
        assertEquals("Shredded Mozzarella",mozzarellaCheese.toString());
        assertEquals("Shredded Parmesan",parmesanCheese.toString());
        assertEquals("Reggiano Cheese",regiannoCheese.toString());
    }

}
