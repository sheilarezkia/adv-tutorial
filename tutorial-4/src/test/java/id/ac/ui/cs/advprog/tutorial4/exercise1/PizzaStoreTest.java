package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;


import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class PizzaStoreTest {
    private Class<?> psClass;

    @Before
    public void setUp() throws Exception {
        psClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore");
    }

    @Test
    public void testPizzaStoreModifier() {
        int pizzaStoreModifier = psClass.getModifiers();
        assertTrue(Modifier.isAbstract(pizzaStoreModifier));
    }

    @Test
    public void testPizzaStoreCreatePizzaMethod() throws Exception {
        Method createPizzaMethod = psClass.getDeclaredMethod("createPizza",
                String.class);
        int methodModifier = createPizzaMethod.getModifiers();

        assertTrue(Modifier.isProtected(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
    }

    @Test
    public void testPizzaStoreOrderPizzaMethod() throws Exception {
        Method orderPizzaMethod = psClass.getDeclaredMethod("orderPizza",
                String.class);
        int methodModifiers = orderPizzaMethod.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }


}
