package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NewYorkPizzaStoreTest {

    private PizzaStore nyPizzaStr;

    @Before
    public void setUp() throws Exception {
        nyPizzaStr = new NewYorkPizzaStore();
    }

    @Test
    public void testCreateCheesePizza(){
        Pizza cheese = nyPizzaStr.orderPizza("cheese");
        assertTrue(cheese instanceof CheesePizza);
        assertEquals("New York Style Cheese Pizza",cheese.getName());
    }

    @Test
    public void testCreateVeggiesPizza(){
        Pizza veg = nyPizzaStr.orderPizza("veggie");
        assertTrue(veg instanceof VeggiePizza);
        assertEquals("New York Style Veggie Pizza",veg.getName());
    }

    @Test
    public void testCreateClamPizza(){
        Pizza clam = nyPizzaStr.orderPizza("clam");
        assertTrue(clam instanceof ClamPizza);
        assertEquals("New York Style Clam Pizza",clam.getName());
    }
}

