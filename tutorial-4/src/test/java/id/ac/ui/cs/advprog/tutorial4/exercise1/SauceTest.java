package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SauceTest {

    private Class<?> sauceClass;
    private Class<?> chiliClass;
    private Class<?> marinaraClass;
    private Class<?> plumTomatoClass;

    @Before
    public void setUp() throws Exception{
        sauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
        chiliClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChiliSauce");
        marinaraClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
        plumTomatoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
    }

    @Test
    public void testSauceIsAPublicInterface() {
        int sauceClassModifiers = sauceClass.getModifiers();

        assertTrue(Modifier.isPublic(sauceClassModifiers));
        assertTrue(Modifier.isInterface(sauceClassModifiers));
    }

    @Test
    public void testSauceToStringMethod() throws Exception {
        Method toStringMethod = sauceClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testChiliSauceImplementsSauce() {
        Collection<Type> chiliInterfaces = Arrays.asList(chiliClass.getInterfaces());

        assertTrue(chiliInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testChiliSauceOverridesToString() throws Exception {
        Method toStringMethod = chiliClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testMarinaraSauceImplementsSauceBehaviour() {
        Collection<Type> marinaraInterfaces = Arrays.asList(marinaraClass.getInterfaces());

        assertTrue(marinaraInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testMarinaraOverridesToString() throws Exception {
        Method toStringMethod = marinaraClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testPlumTomatoImplementsSauce() {
        Collection<Type> tomatoInterfaces = Arrays.asList(plumTomatoClass.getInterfaces());

        assertTrue(tomatoInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce")));
    }

    @Test
    public void testPlumTomatoOverridesToString() throws Exception {
        Method toStringMethod = plumTomatoClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }
}
