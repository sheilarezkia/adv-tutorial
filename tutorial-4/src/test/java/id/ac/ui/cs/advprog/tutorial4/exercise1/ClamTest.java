package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClamTest {

    private Class<?> clamClass;
    private Class<?> freshClamClass;
    private Class<?> frozenClamClass;
    private Class<?> friedClamClass;

    @Before
    public void setUp() throws Exception {
        clamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams");
        freshClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams");
        frozenClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams");
        friedClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FriedClams");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int clamClassModifiers = clamClass.getModifiers();

        assertTrue(Modifier.isPublic(clamClassModifiers));
        assertTrue(Modifier.isInterface(clamClassModifiers));
    }

    @Test
    public void testClamsHasToStringMethod() throws Exception {
        Method toStringMethod = clamClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testFreshClamClamsBehavior() {
        Collection<Type> freshClamsInterfaces = Arrays.asList(freshClamClass.getInterfaces());

        assertTrue(freshClamsInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams")));
    }

    @Test
    public void testFreshClamOverridesToString() throws Exception {
        Method toStringMethod = freshClamClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testFrozenClamClamsBehavior() {
        Collection<Type> frozenClamInterfaces = Arrays.asList(frozenClamClass.getInterfaces());

        assertTrue(frozenClamInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams")));
    }

    @Test
    public void testFrozenClamOverridesToString() throws Exception {
        Method toStringMethod = frozenClamClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testFriedClamClamsBehavior() {
        Collection<Type> friedClamInterfaces = Arrays.asList(friedClamClass.getInterfaces());

        assertTrue(friedClamInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams")));
    }

    @Test
    public void testFriedClamOverridesToString() throws Exception {
        Method toStringMethod = friedClamClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }
}
