package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DepokPizzaStoreTest {

    private PizzaStore dpkPizzaStr;

    @Before
    public void setUp() throws Exception {
        dpkPizzaStr = new DepokPizzaStore();
    }

    @Test
    public void testCreateCheesePizza() {
        Pizza cheese = dpkPizzaStr.orderPizza("cheese");
        assertTrue(cheese instanceof CheesePizza);
        assertEquals("Depok Style Cheese Pizza", cheese.getName());
    }

    @Test
    public void testCreateVeggiesPizza() {
        Pizza veggie = dpkPizzaStr.orderPizza("veggie");
        assertTrue(veggie instanceof VeggiePizza);
        assertEquals("Depok Style Veggie Pizza", veggie.getName());
    }

    @Test
    public void testCreateClamPizza() {
        Pizza clam = dpkPizzaStr.orderPizza("clam");
        assertTrue(clam instanceof ClamPizza);
        assertEquals("Depok Style Clam Pizza", clam.getName());
    }
}
