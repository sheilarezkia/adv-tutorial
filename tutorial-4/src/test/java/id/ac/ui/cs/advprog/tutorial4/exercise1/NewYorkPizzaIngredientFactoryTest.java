package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class NewYorkPizzaIngredientFactoryTest {
    private Class<?> nyPizzaIngFactory;

    @Before
    public void setUp() throws Exception {
        nyPizzaIngFactory = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory");
    }

    @Test
    public void testNYPizzaIngFactoryPizzaIngFactoryBehavior() {
        Collection<Type> nyPizzaFactoryInterfaces = Arrays.asList(nyPizzaIngFactory.getInterfaces());

        assertTrue(nyPizzaFactoryInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory")));

    }

    @Test
    public void testNYPizzaOverridesMethodCreateDough() throws Exception {
        Method createDoughMethod = nyPizzaIngFactory.getDeclaredMethod("createDough");
        int createDoughModifier = createDoughMethod.getModifiers();

        assertTrue(Modifier.isPublic(createDoughModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", createDoughMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testNYPizzaOverridesMethodCreateSauce() throws Exception {
        Method createSauceMethod = nyPizzaIngFactory.getDeclaredMethod("createSauce");
        int createSauceModifier = createSauceMethod.getModifiers();

        assertTrue(Modifier.isPublic(createSauceModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", createSauceMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testNYPizzaOverridesMethodCreateCheese() throws Exception {
        Method createCheeseMethod = nyPizzaIngFactory.getDeclaredMethod("createCheese");
        int createCheeseModifier = createCheeseMethod.getModifiers();

        assertTrue(Modifier.isPublic(createCheeseModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", createCheeseMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testNYPizzaOverridesMethodCreateVeggies() throws Exception {
        Method createVegMethod = nyPizzaIngFactory.getDeclaredMethod("createVeggies");
        int createVegModifier = createVegMethod.getModifiers();

        assertTrue(Modifier.isPublic(createVegModifier));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", createVegMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testNYPizzaOverridesMethodCreateClams() throws Exception {
        Method createClam = nyPizzaIngFactory.getDeclaredMethod("createClam");
        int methodModifiers = createClam.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", createClam.getGenericReturnType().getTypeName());

    }

}
