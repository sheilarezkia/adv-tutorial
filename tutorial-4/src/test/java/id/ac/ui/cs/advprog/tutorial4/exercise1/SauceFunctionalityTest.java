package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SauceFunctionalityTest {

    private Sauce chili;
    private Sauce marinara;
    private Sauce plumTomato;

    @Before
    public void setUp() throws Exception {
        chili = new ChiliSauce();
        marinara = new MarinaraSauce();
        plumTomato = new PlumTomatoSauce();

    }

    @Test
    public void testSauceOutput(){
        assertEquals("Chili Sauce", chili.toString());
        assertEquals("Marinara Sauce", marinara.toString());
        assertEquals("Tomato sauce with plum tomatoes", plumTomato.toString());
    }

}
