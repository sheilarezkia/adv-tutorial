package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;



import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VeggieFunctionalityTest {

    private Veggies blackOlives;
    private Veggies eggplant;
    private Veggies garlic;
    private Veggies onion;
    private Veggies redPepper;
    private Veggies spinach;
    private Veggies tomato;
    private Veggies mushrooms;

    @Before
    public void setUp() throws Exception {
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushrooms = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
        tomato = new Tomato();
    }

    @Test
    public void testVeggiesOutput() {
        assertEquals("Black Olives", blackOlives.toString());
        assertEquals("Eggplant", eggplant.toString());
        assertEquals("Garlic", garlic.toString());
        assertEquals("Mushrooms", mushrooms.toString());
        assertEquals("Onion", onion.toString());
        assertEquals("Red Pepper", redPepper.toString());
        assertEquals("Spinach", spinach.toString());
        assertEquals("Tomatoes", tomato.toString());
    }

}
