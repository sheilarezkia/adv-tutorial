package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VeggieTest {

    private Class<?> veggieClass;
    private Class<?> blackOlivesClass;
    private Class<?> eggplantClass;
    private Class<?> garlicClass;
    private Class<?> mushroomClass;
    private Class<?> onionClass;
    private Class<?> redPepperClass;
    private Class<?> spinachClass;
    private Class<?> tomatoClass;

    @Before
    public void setUp() throws Exception {
        veggieClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
        blackOlivesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives");
        eggplantClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant");
        garlicClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic");
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom");
        onionClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion");
        redPepperClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper");
        spinachClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach");
        tomatoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato");
    }

    @Test
    public void testVeggieIsAPublicInterface() {
        int vegClassModifier = veggieClass.getModifiers();

        assertTrue(Modifier.isPublic(vegClassModifier));
        assertTrue(Modifier.isInterface(vegClassModifier));
    }

    @Test
    public void vegToStringMethod() throws Exception {
        Method toStringMethod = veggieClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testBlackOlvBehaviourMatchesVeg() {
        Collection<Type> blackOlvInterfaces = Arrays.asList(blackOlivesClass.getInterfaces());

        assertTrue(blackOlvInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testBlackOlvOverridesToString() throws Exception {
        Method toStringMethod = blackOlivesClass.getDeclaredMethod("toString");
        int toStringModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testEggplantsBehaviourMatchesVeg() {
        Collection<Type> eggplantsInterfaces = Arrays.asList(eggplantClass.getInterfaces());

        assertTrue(eggplantsInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testEggplantOverridesToString() throws Exception {
        Method toStringMethod = eggplantClass.getDeclaredMethod("toString");
        int toStringModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStringModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testGarlicAVegBehavior() {
        Collection<Type> garlicInterfaces = Arrays.asList(garlicClass.getInterfaces());

        assertTrue(garlicInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testGarlicOverridesToString() throws Exception {
        Method toStringMethod = garlicClass.getDeclaredMethod("toString");
        int toStrModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testMushroomVegBehavior() {
        Collection<Type> mushroomInterfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(mushroomInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testMushroomOverridesToString() throws Exception {
        Method toStringMethod = mushroomClass.getDeclaredMethod("toString");
        int toStrModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testOnionVegBehavior() {
        Collection<Type> onionInterfaces = Arrays.asList(onionClass.getInterfaces());

        assertTrue(onionInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testOnionOverridesToString() throws Exception {
        Method toStringMethod = onionClass.getDeclaredMethod("toString");
        int toStrModifier = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifier));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


    @Test
    public void testRedPepperVegBehavior() {
        Collection<Type> pepperInterfaces = Arrays.asList(redPepperClass.getInterfaces());

        assertTrue(pepperInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testRedPepperOverridesToString() throws Exception {
        Method toStringMethod = redPepperClass.getDeclaredMethod("toString");
        int toStrModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testSpinachVegBehavior() {
        Collection<Type> spinachInterfaces = Arrays.asList(spinachClass.getInterfaces());

        assertTrue(spinachInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testSpinachOverridesToString() throws Exception {
        Method toStringMethod = spinachClass.getDeclaredMethod("toString");
        int toStrModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }

    @Test
    public void testTomatoVegBehavior() {
        Collection<Type> tomatoInterfaces = Arrays.asList(tomatoClass.getInterfaces());

        assertTrue(tomatoInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies")));
    }

    @Test
    public void testTomatoOverridesToString() throws Exception {
        Method toStringMethod = tomatoClass.getDeclaredMethod("toString");
        int toStrModifiers = toStringMethod.getModifiers();

        assertTrue(Modifier.isPublic(toStrModifiers));
        assertEquals("java.lang.String", toStringMethod.getGenericReturnType().getTypeName());

    }


}
