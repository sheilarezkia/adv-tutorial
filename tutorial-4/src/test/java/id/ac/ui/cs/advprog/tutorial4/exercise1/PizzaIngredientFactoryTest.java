package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Modifier;

public class PizzaIngredientFactoryTest {
    private Class<?> pizzaIngFactoryClass;

    @Before
    public void setUp() throws Exception {
        pizzaIngFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory");
    }

    @Test
    public void testPizzaIngredientFactoryIsAPublicInterface() {
        int ingFactoryModifiers = pizzaIngFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(ingFactoryModifiers));
        assertTrue(Modifier.isInterface(ingFactoryModifiers));
    }


}
