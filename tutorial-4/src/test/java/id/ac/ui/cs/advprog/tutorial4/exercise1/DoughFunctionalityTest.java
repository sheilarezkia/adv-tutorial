package id.ac.ui.cs.advprog.tutorial4.exercise1;

        import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
        import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
        import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
        import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
        import org.junit.Before;
        import org.junit.Test;

        import static org.junit.Assert.assertEquals;

public class DoughFunctionalityTest {

    private Dough noCrust;
    private Dough thinCrust;
    private Dough thickCrust;

    @Before
    public void setUp() throws Exception {
        noCrust = new NoCrustDough();
        thinCrust= new ThinCrustDough();
        thickCrust= new ThickCrustDough();

    }

    @Test
    public void testClamsOutput(){
        assertEquals("No Crust Dough",noCrust.toString());
        assertEquals("ThickCrust style extra thick crust dough",thickCrust.toString());
        assertEquals("Thin Crust Dough",thinCrust.toString());
    }

}
