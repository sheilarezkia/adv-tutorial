package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        //TODO Implement
        this.name = name;
        if (salary >= 30000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }

        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}