package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

/**
 * Created by sheilarezkia on 3/2/18.
 */
public class Main {
    public static void main(String[] args) {
        Food classicBunBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();

        classicBunBurger = FillingDecorator.CHICKEN_MEAT.addFillingToBread(classicBunBurger);

        classicBunBurger = FillingDecorator.CHEESE.addFillingToBread(
                classicBunBurger);

        classicBunBurger = FillingDecorator.TOMATO.addFillingToBread(
                classicBunBurger);

        classicBunBurger = FillingDecorator.LETTUCE.addFillingToBread(
                classicBunBurger);

        classicBunBurger = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                classicBunBurger);

        System.out.println("Your Food Details: " + classicBunBurger.getDescription());
        System.out.println("Total Price: " + classicBunBurger.cost());
        System.out.println();


        Food nonCrispyChickenSandwich =
                BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();

        nonCrispyChickenSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                        nonCrispyChickenSandwich);

        nonCrispyChickenSandwich =
                FillingDecorator.LETTUCE.addFillingToBread(
                        nonCrispyChickenSandwich);

        nonCrispyChickenSandwich =
                FillingDecorator.CHEESE.addFillingToBread(
                        nonCrispyChickenSandwich);

        nonCrispyChickenSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        nonCrispyChickenSandwich);

        System.out.println("Your Food Details: " + nonCrispyChickenSandwich.getDescription());
        System.out.println("Total Price: " + nonCrispyChickenSandwich.cost());
    }
}
