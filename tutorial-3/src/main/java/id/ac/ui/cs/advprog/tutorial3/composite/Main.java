package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;


/**
 * Created by sheilarezkia on 3/2/18.
 */
public class Main {
    public static void main(String[] args) {

        Company company = new Company();
        Ceo companyCeo = new Ceo("Ada", 250000.00);
        company.addEmployee(companyCeo);

        Cto companyCto = new Cto("Bob", 150000.00);
        company.addEmployee(companyCto);

        BackendProgrammer backEnd = new BackendProgrammer("Chloe", 99000.00);
        company.addEmployee(backEnd);

        FrontendProgrammer frontEnd = new FrontendProgrammer("Dory", 70000.00);
        company.addEmployee(frontEnd);

        UiUxDesigner uiux = new UiUxDesigner("Elaine", 92000.00);
        company.addEmployee(uiux);

        NetworkExpert networkExpert = new NetworkExpert("Frank", 100000.00);
        company.addEmployee(networkExpert);

        SecurityExpert security = new SecurityExpert("George", 100000.00);
        company.addEmployee(security);

        System.out.println("Company net salary is: " + company.getNetSalaries());

        System.out.println("Here's the employees list: ");
        List<Employees> allEmployees = company.getAllEmployees();

        for (Employees employee : allEmployees) {
            System.out.println("Name: " + employee.getName());
            System.out.println("Role: " + employee.getRole());
            System.out.println("Salary: " + employee.getSalary());
            System.out.println();
        }
    }
}
