package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        //TODO Implement
        this.name = name;
        if (salary >= 90000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }

        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}