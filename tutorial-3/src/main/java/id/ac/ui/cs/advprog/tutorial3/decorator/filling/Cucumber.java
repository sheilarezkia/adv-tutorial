package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        description = food.getDescription() + ", adding cucumber";
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return Double.sum(this.food.cost(),0.40);
    }
}
