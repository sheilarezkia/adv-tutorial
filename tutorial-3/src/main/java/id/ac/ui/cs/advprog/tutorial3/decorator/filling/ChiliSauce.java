package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        description = food.getDescription() + ", adding chili sauce";
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return Double.sum(this.food.cost(),0.30);
    }
}
