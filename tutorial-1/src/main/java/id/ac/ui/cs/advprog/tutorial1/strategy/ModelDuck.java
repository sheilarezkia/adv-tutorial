package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
	public ModelDuck() {
		setQuackBehavior(new Quack());
		setFlyBehavior(new FlyWithWings());
	}

	public void display() {
		System.out.println("I'm a model duck");
	}  
}
